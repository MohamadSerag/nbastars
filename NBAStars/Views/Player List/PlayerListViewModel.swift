import CoreData
import SwiftUI

protocol PlayerListViewModeling: ObservableObject {
    var players: [PlayerModel] { get }
    func delete(at index: Int)
    func loadSampleData()
}

class PlayerListViewModel: PlayerListViewModeling {
    var players: [PlayerModel] { moc.fetchPlayers() }
    private var moc: NSManagedObjectContext
    
    init(moc: NSManagedObjectContext = UIApplication.viewContext) {
        self.moc = moc
    }
    
    func delete(at index: Int) {
        objectWillChange.send()
        moc.delete(moc.fetchPlayers()[index])
    }
    
    func loadSampleData() {
        objectWillChange.send()
        let team = TeamModel(context: moc)
        for (index, name) in ["Jordan", "Johnson", "Bird"].enumerated() {
            let player = PlayerModel(context: moc)
            player.name = name
            player.photo = UIImage(systemName: "person.fill")!.jpegData(compressionQuality: 1)!
            player.displayOrder = index
            player.team = team
        }
    }
}

extension NSManagedObjectContext {
    func fetchPlayers() -> [PlayerModel] {
        try! fetch(PlayerModel.sortedFetchRequest())
    }
}
