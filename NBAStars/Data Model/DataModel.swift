import CoreData

protocol Listable where Self: NSManagedObject {
    static func sortedFetchRequest(predicate: NSPredicate?) -> NSFetchRequest<Self>
    var displayOrder: Int { get set }
}

extension Listable {
    static func sortedFetchRequest(predicate: NSPredicate? = nil) -> NSFetchRequest<Self> {
        let req: NSFetchRequest<Self> = NSFetchRequest(entityName: String(describing: Self.self))
        req.predicate = predicate
        req.sortDescriptors = [NSSortDescriptor(key: "displayOrder", ascending: true)]
        return req
    }
}

final class TeamModel: NSManagedObject, Identifiable, Listable {
    @NSManaged var name: String
    @NSManaged var logo: Data
    @NSManaged var displayOrder: Int
    @NSManaged var playerSet: NSSet
    
    var players: [PlayerModel] { playerSet.map { $0 as! PlayerModel } }
    
    func update(with info: [TeamModelKey:Any]) {
        if let newName = info[.name] as? String { name = newName }
        if let newLogo = info[.logo] as? Data { logo = newLogo }
        if let newDO = info[.displayOrder] as? Int { displayOrder = newDO }
    }
}

final class PlayerModel: NSManagedObject, Identifiable, Listable {
    @NSManaged var name: String
    @NSManaged var photo: Data
    @NSManaged var displayOrder: Int
    @NSManaged var team: TeamModel
    
    func update(with info: [PlayerModelKey:Any]) {
        if let newName = info[.name] as? String { name = newName }
        if let newPhoto = info[.photo] as? Data { photo = newPhoto }
        if let newDO = info[.displayOrder] as? Int { displayOrder = newDO }
        if let newTeam = info[.team] as? TeamModel { team = newTeam }
    }
}

enum TeamModelKey {
    case name, logo, displayOrder
}

enum PlayerModelKey {
    case name, photo, displayOrder, team
}
